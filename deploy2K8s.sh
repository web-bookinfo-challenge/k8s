#!/bin/bash

# Set the root folder path to script execution folder path
root_folder="$(cd "$(dirname "$0")" && pwd)"

# Set the folder paths in the desired priority order (adjust as needed)
priority_folders=(
  "$root_folder/mongodb"
  "$root_folder/ratings"
  "$root_folder/reviews"
  "$root_folder/details"
  "$root_folder/productpage"
)

# Function to apply each YAML file using kubectl
apply_yaml_files() {
  local file_path=$1
  echo "Applying $file_path"
  kubectl apply -f "$file_path"
}

# Check if kubectl is installed
if ! command -v kubectl &> /dev/null; then
  echo "kubectl not found. Please install kubectl and try again."
  exit 1
fi

# ANSI color codes for formatting
color_reset='\033[0m'
color_yellow='\033[1;33m'

# Function to print colored lines for separation
print_separator() {
  printf "${color_yellow}%s${color_reset}\n" "--------------------------------------------------------"
}

# Iterate over the priority folders and find YAML files to apply
for folder_path in "${priority_folders[@]}"; do
  # Check if the folder exists
  if [ -d "$folder_path" ]; then
    # Find YAML files recursively in the folder and its subdirectories
    yaml_files=()
    while IFS= read -r -d '' yaml_file; do
      yaml_files+=("$yaml_file")
    done < <(find "$folder_path" -type f -name "*.yaml" -print0)

    # Display the list of YAML files and their contents for a dry run
    print_separator
    echo -e "${color_yellow}Dry Run: The following YAML files will be applied from $folder_path:${color_reset}"
    print_separator

    for yaml_file in "${yaml_files[@]}"; do
      cat "$yaml_file"
      print_separator
    done

    # Prompt for confirmation
    read -p "Do you want to proceed with applying the YAML files from $folder_path? (y/n): " response

    # Check user's response
    if [[ $response =~ ^[Yy]$ ]]; then
      for yaml_file in "${yaml_files[@]}"; do
        apply_yaml_files "$yaml_file"
      done
      echo "All YAML files in $folder_path have been applied."
    else
      echo "Operation canceled for $folder_path. No changes have been made."
    fi
  else
    echo "Folder not found: $folder_path. Skipping..."
  fi
done
