# WebBookInfo Challenge K8s deployment script

This bash script allows you to apply WebBookInfo challenge's YAML files to a Kubernetes cluster. It recursively searches for YAML files in a specified root folder and its subdirectories and applies them using `kubectl`.

## Prerequisites

- `kubectl` must be installed and configured to connect to your Kubernetes cluster.
- Kubernetes version `>=1.19`

## Getting Started

1. Clone the repository to your local machine:

   ```bash
   git clone https://gitlab.com/web-bookinfo-challenge/k8s.git
   cd k8s
   ```

2. Ensure that the script `deploy2k8s.sh` is executable:

   ```bash
   chmod +x deploy2k8s.sh
   ```

3. Review the YAML files to be applied:

   Run the script in dry-run mode to see the list of YAML files that will be applied:

   ```bash
   ./deploy2k8s.sh --dry-run
   ```

   This will display the YAML files that will be applied to your cluster. Review the list and confirm that they are correct before proceeding.

4. Apply the YAML files:

   Once you have reviewed the list of YAML files, run the script without the `--dry-run` option to apply the changes to your Kubernetes cluster:

   ```bash
   ./deploy2k8s.sh
   ```

   The script will prompt you to confirm whether you want to proceed with applying the YAML files. Type `y` or `Y` to proceed, or any other input to cancel the operation.

   **Caution:** Applying YAML files will create or modify resources in your cluster. Make sure you have reviewed the changes and that they match your intended configuration.

## Customization

- If your YAML files are located in a different folder, modify the `root_folder` variable in the script (`deploy2k8s.sh`) to point to the desired location.
- Adjust any other variables or script parameters as needed to match your specific environment.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
